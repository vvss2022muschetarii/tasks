package tasks.services;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import tasks.model.ArrayTaskList;
import tasks.model.Task;
import tasks.model.TasksOperations;

import java.util.Date;
import java.util.List;

/*
service class
 */
public class TasksService {

//    ObservableList<Task> taskObservableList;
    ArrayTaskList tasks;

    public TasksService(ArrayTaskList tasks){
//        taskObservableList = FXCollections.observableArrayList(tasks.getAll());
        this.tasks = tasks;
    }

    public ArrayTaskList getTasks() {
        return tasks;
    }

    public ObservableList<Task> getObservableList(){
//        return taskObservableList;
        return FXCollections.observableArrayList(tasks.getAll());
    }
    public String getIntervalInHours(Task task){
        int seconds = task.getRepeatInterval();
        int minutes = seconds / DateService.SECONDS_IN_MINUTE;
        int hours = minutes / DateService.MINUTES_IN_HOUR;
        minutes = minutes % DateService.MINUTES_IN_HOUR;
        return formTimeUnit(hours) + ":" + formTimeUnit(minutes);//hh:MM
    }
    public String formTimeUnit(int timeUnit){
        StringBuilder sb = new StringBuilder();
        if (timeUnit < 10) sb.append("0");
        if (timeUnit == 0) sb.append("0");
        else {
            sb.append(timeUnit);
        }
        return sb.toString();
    }

    public int parseFromStringToSeconds(String stringTime){//hh:MM
        String[] units = stringTime.split(":");
        int hours = Integer.parseInt(units[0]);
        int minutes = Integer.parseInt(units[1]);
        int result = (hours * DateService.MINUTES_IN_HOUR + minutes) * DateService.SECONDS_IN_MINUTE;
        return result;
    }

    public Iterable<Task> filterTasks(Date start, Date end){
        TasksOperations tasksOps = new TasksOperations(getObservableList());
        Iterable<Task> filtered = tasksOps.incoming(start,end);
        //Iterable<Task> filtered = tasks.incoming(start, end);

        return filtered;
    }

    public void add(Task collectedFieldsTask) {
//        taskObservableList.add(collectedFieldsTask);
        tasks.add(collectedFieldsTask);
    }

    public void addTask(String description, Date startDate, Date endDate, int interval)
    {
        this.add(new Task(description, startDate, endDate, interval));
    }

    public void update(Task collectedFieldsTask) {
//        for (int i = 0; i < taskObservableList.size(); i++){
        for (int i = 0; i < tasks.size(); i++){
            if (collectedFieldsTask.equals(tasks.get(i))){
                tasks.set(i, collectedFieldsTask);
            }
        }
    }
}
