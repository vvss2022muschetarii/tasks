package tasks.services;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import tasks.model.ArrayTaskList;
import tasks.model.Task;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

@TestMethodOrder(MethodOrderer.Random.class)
class TasksServiceTest {

    private static final Date END_DATE = new Date("2022/03/19");
    private static final Date START_DATE = new Date("2022/03/17");
    static ArrayTaskList tasksList;
    static TasksService tasksService;
    static Task task;
    @BeforeAll
    static void initialSetup()
    {
        tasksList = new ArrayTaskList();
        tasksService = new TasksService(tasksList);
    }

    @BeforeEach
    void setUp() {
        task = null;
    }

    @AfterEach
    void tearDown() {
        if(tasksService.getTasks().size()>0)
            tasksService.getTasks().remove(tasksService.getTasks().get(0));
    }

    @Test
    @DisplayName("Test valid => se adauga task")
    void testValid() throws ParseException {
        validTasksProvider().forEach(arg -> {
            task = new Task((String)arg.get()[0], START_DATE, END_DATE, (Integer) arg.get()[1]);
            tasksService.add(task);
            List<Task> tasksList = tasksService.getObservableList();
            Assertions.assertEquals(1, tasksList.size());
            Assertions.assertEquals(task, tasksList.get(0));
            tearDown();
        });
    }

    @ParameterizedTest
    @DisplayName("Test valid => se adauga task")
    @MethodSource(value = "validTasksProvider")
    @Disabled("not runnning on jenkins")
    void testValidDisabled(String title, int interval) throws ParseException {
        task = new Task(title, START_DATE, END_DATE, interval);
        tasksService.add(task);
        List<Task> tasksList = tasksService.getObservableList();
        Assertions.assertEquals(1, tasksList.size());
        Assertions.assertEquals(task, tasksList.get(0));
    }

    private static Stream<Arguments> validTasksProvider()
    {
        return Stream.of(
            Arguments.of("Lorem ipsum dolor sit amet.", 130),
            Arguments.of("Sed in aliquet quam. Ut.", 207),
            Arguments.of("Vestibulum condimentum nibh convallis nisl.", 276),
            Arguments.of("Cras quis blandit neque. Nulla.", 345),
            Arguments.of("a", 1),
            Arguments.of("ab", 1),
            Arguments.of("M01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123", 1),
            Arguments.of("M0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012", 1),
            Arguments.of("random", 1),
            Arguments.of("random", 2),
            Arguments.of("random", 5)
        );
    }

    @Test
    @DisplayName("Test invalid descriere 1 (tip invalid) => eroare de compilare")
    @Disabled("Compile error")
    void testInvalidDescriere_1()
    {
        // task = new Task(24, new Date("2022/03/17"), new Date("2022/03/19"), 60);
        // tasksService.add(task);
        // List<Task> tasksList = tasksService.getObservableList();
        // Assertions.assertEquals(1, tasksList.size());
        // Assertions.assertEquals(task, tasksList.get(0));
    }
 
    @Test
    @RepeatedTest(10)
    @Timeout(value = 100, unit=TimeUnit.MILLISECONDS)
    @DisplayName("Test invalid descriere 2 => se arunca IllegalArgumentException")
    void testInvalidDescriere_2()
    {
        Assertions.assertThrows(IllegalArgumentException.class, ()->{
                tasksService.addTask(null, START_DATE, END_DATE, 120);
            },
            "description cannot be null"
        );
    }

    @Test
    @DisplayName("Test invalid descriere 3 => se arunca IllegalArgumentException")
    void testInvalidDescriere_3()
    {
        Assertions.assertThrows(IllegalArgumentException.class, ()->{
                tasksService.addTask("", START_DATE, END_DATE, 120);
            },
            "description should not be empty"
        );
    }

    @Test
    @DisplayName("Test invalid descriere 1 (tip invalid) => eroare de compilare")
    @Disabled("Compile error")
    void testInvalidInterval_1()
    {
        // task = new Task("Sed lacinia eget massa vitae.", new Date("2022/03/17"), new Date("2022/03/19"), "salut");
        // tasksService.add(task);
        // List<Task> tasksList = tasksService.getObservableList();
        // Assertions.assertEquals(1, tasksList.size());
        // Assertions.assertEquals(task, tasksList.get(0));
    }

    @Test
    @DisplayName("Test invalid interval 2 => se arunca IllegalArgumentException")
    void testInvalidInterval_2()
    {
        Assertions.assertThrows(IllegalArgumentException.class, ()->{
                tasksService.addTask("", START_DATE, END_DATE, 0);
            },
            "interval should me > 1"
        );
    }

    @Test
    @DisplayName("Test invalid descriere 4 => se arunca IllegalArgumentException")
    void testInvalidDescriere_4()
    {
        Assertions.assertThrows(IllegalArgumentException.class, ()->{
                tasksService.addTask("", START_DATE, END_DATE, 1);
            },
            "description should not be empty"
        );
    }

    @Test
    @DisplayName("Test invalid descriere 5 => eroare de compilare (constantele string nu pot fi mai lungi de 65535 de caractere)")
    @Disabled("compile error")
    void testInvalidDescriere_5() throws ParseException {
        
        // String title = "M012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234";
        // task = new Task(title, START_DATE, END_DATE, 1);
        // tasksService.add(task);
        // List<Task> tasksList = tasksService.getObservableList();
        // Assertions.assertEquals(1, tasksList.size());
        // Assertions.assertEquals(task, tasksList.get(0));
    }

    @Test
    @DisplayName("Test invalid interval 3 => se arunca IllegalArgumentException")
    void testInvalidInterval_3()
    {
        Assertions.assertThrows(IllegalArgumentException.class, ()->{
                tasksService.addTask("random", START_DATE, END_DATE, 0);
            },
            "interval should me > 1"
        );
    }

    @Test
    @DisplayName("Test invalid descriere 6 => se arunca IllegalArgumentException")
    void testInvalidDescriere_6()
    {
        Assertions.assertThrows(IllegalArgumentException.class, ()->{
                tasksService.addTask(null, START_DATE, END_DATE, 1);
            },
            "description cannot be null"
        );
    }

    @Test
    @DisplayName("Test invalid descriere 7 => se arunca IllegalArgumentException")
    void testInvalidDescriere_7()
    {
        Assertions.assertThrows(IllegalArgumentException.class, ()->{
                tasksService.addTask("random", START_DATE, END_DATE, -1);
            },
            "description cannot be null"
        );
    }

    @Test
    @DisplayName("Test invalid descriere 4 (tip invalid) => eroare de compilare")
    @Disabled("Compile error")
    void testInvalidInterval_4()
    {
        // task = new Task("random", new Date("2022/03/17"), new Date("2022/03/19"), "a");
        // tasksService.add(task);
        // List<Task> tasksList = tasksService.getObservableList();
        // Assertions.assertEquals(1, tasksList.size());
        // Assertions.assertEquals(task, tasksList.get(0));
    }
}