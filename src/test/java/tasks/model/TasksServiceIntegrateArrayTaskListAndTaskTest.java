package tasks.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tasks.services.TasksService;

import java.util.Date;

public class TasksServiceIntegrateArrayTaskListAndTaskTest {
    private ArrayTaskList arrayTaskList;
    private TasksService tasksService;
    private Task task = new Task("taskDescription", new Date());

    @BeforeEach
    void setUp() {
        arrayTaskList = new ArrayTaskList();
        tasksService = new TasksService(arrayTaskList);
    }

    @Test
    void testAddValid() {
        tasksService.add(task);
        Assertions.assertEquals(1, tasksService.getTasks().size());
    }

    @Test
    void testUpdateValid() {
        String newDescription = "Ceva";
        tasksService.add(task);
        tasksService.add(task);
        task.setDescription(newDescription);
        tasksService.update(task);
        Assertions.assertEquals(newDescription, tasksService.getTasks().getTask(0).getDescription());
    }
}
