package tasks.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import tasks.services.TasksService;

import java.util.Date;

@ExtendWith(MockitoExtension.class)
public class TasksServiceIntegrateArrayTaskListTest {
    private ArrayTaskList arrayTaskList;
    private TasksService tasksService;
    @Mock
    private Task task;

    @BeforeEach
    void setUp() {
        arrayTaskList = new ArrayTaskList();
        tasksService = new TasksService(arrayTaskList);
    }

    @Test
    void testAddValid() {
        tasksService.add(task);
        Assertions.assertEquals(1, tasksService.getTasks().size());
    }

    @Test
    void testUpdateValid() {
        String newDescription = "Ceva";
        tasksService.add(task);
        tasksService.add(task);
        task.setDescription(newDescription);
        tasksService.update(task);
        Mockito.when(task.getDescription()).thenReturn(newDescription);
        Assertions.assertEquals(newDescription, tasksService.getTasks().getTask(0).getDescription());
    }
}
