package tasks.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import tasks.services.TasksService;

import java.util.Date;

@ExtendWith(MockitoExtension.class)
public class TasksServiceUnitTest {
    @Mock
    private ArrayTaskList arrayTaskList;
    @InjectMocks
    private TasksService tasksService;
    @Mock
    private Task task;

//    @BeforeEach
//    void setUp() {
//        arrayTaskList = Mockito.mock(ArrayTaskList.class);
//        tasksService = new TasksService(arrayTaskList);
//    }

    @Test
    void testAddValid() {
        tasksService.add(task);
        Mockito.when(arrayTaskList.size()).thenReturn(1);
        Assertions.assertEquals(1, tasksService.getTasks().size());
    }

    @Test
    void testUpdateValid() {
        String newDescription = "Ceva";
        tasksService.add(task);
        tasksService.add(task);
        task.setDescription(newDescription);
        tasksService.update(task);
        Mockito.when(arrayTaskList.getTask(0)).thenReturn(task);
        Mockito.when(task.getDescription()).thenReturn(newDescription);
        Assertions.assertEquals(newDescription, tasksService.getTasks().getTask(0).getDescription());
    }
}
