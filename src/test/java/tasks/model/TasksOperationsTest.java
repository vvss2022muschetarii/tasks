package tasks.model;

import javafx.collections.FXCollections;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

class TasksOperationsTest {

    private final static String DESCRIPTION = "Nunc malesuada libero sed congue convallis.";
    private final static Date start = new Date(2022, Calendar.MARCH, 29);
    private final static Date end = new Date(2022, Calendar.MARCH, 31);
    private final static int N = 50;
    private static TasksOperations tasksOperations;

    @BeforeAll
    static void initialSetup() {
        tasksOperations = new TasksOperations(FXCollections.observableArrayList());
    }

    private static Stream<Arguments> validArgumentsProvider() {
        return Stream.of(
                Arguments.of(start, end, new ArrayList<Task>(), 0),
                Arguments.of(start, end, createTasks(1, new Date(start.getTime() - TimeUnit.DAYS.toMillis(1))), 0),
                Arguments.of(start, end, createTasks(2, new Date(end.getTime() + TimeUnit.DAYS.toMillis(1))), 0),
                Arguments.of(start, end, createTasks(N, end), N),
                Arguments.of(start, end, createTasks(N, new Date(end.getTime() - TimeUnit.DAYS.toMillis(1))), N)
        );
    }

    private static ArrayList<Task> createTasks(int noTasks, Date date) {
        ArrayList<Task> tasks = new ArrayList<>();
        for (int i = 0; i < noTasks; i++) {
            tasks.add(new Task(DESCRIPTION, date, true));
        }
        return tasks;
    }

    @ParameterizedTest
    @MethodSource(value = "validArgumentsProvider")
    @DisplayName("Test valid => se filtreaza taskurile")
    @Disabled
    void testValidDisabled(Date start, Date end, ArrayList<Task> tasks, int expectedSize) {
        tasksOperations.tasks = tasks;
        Iterable<Task> filteredTasks = tasksOperations.incoming(start, end);
        Assertions.assertEquals(getSize(filteredTasks), expectedSize);
    }

    @Test
    @DisplayName("Test valid => se filtreaza taskurile")
    void testValid() {
        validArgumentsProvider().forEach(arg -> {
            tasksOperations.tasks = (ArrayList<Task>) arg.get()[2];
            Iterable<Task> filteredTasks = tasksOperations.incoming((Date) arg.get()[0], (Date) arg.get()[1]);
            Assertions.assertEquals(getSize(filteredTasks), arg.get()[3]);
        });
    }

    @Test
    @DisplayName("Test invalid: start == null => se arunca NullPointerException")
    void testInvalid_StartNULL() {
        Assertions.assertThrows(NullPointerException.class, () -> tasksOperations.incoming(null, end),
                "start date must not be null"
        );
    }

    @Test
    @DisplayName("Test invalid: end == null => se arunca NullPointerException")
    void testInvalid_EndNULL() {
        Assertions.assertThrows(NullPointerException.class, () -> tasksOperations.incoming(start, null),
                "end date must not be null"
        );
    }

    @Test
    @DisplayName("Test invalid: start == null, end == null => se arunca NullPointerException")
    void testInvalid_StartNULL_EndNULL() {
        Assertions.assertThrows(NullPointerException.class, () -> tasksOperations.incoming(null, null),
                "start and end dates must not be null"
        );
    }

    @Test
    @DisplayName("Test invalid: end date este inainte de start date => se arunca IllegalArgumentException")
    void testInvalid_EndBeforeStart() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> tasksOperations.incoming(end, start),
                "end date must be after start date"
        );
    }

    private int getSize(Iterable<Task> tasks) {
        int counter = 0;
        for (Task ignored : tasks) {
            counter++;
        }
        return counter;
    }
}