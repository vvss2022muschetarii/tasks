package tasks.model;

import org.junit.jupiter.api.*;

import java.text.ParseException;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class TaskTest {

    private Task task;
    private static final String TASK_DESCRIPTION = "new task";

    @BeforeEach
    void setUp() {
        try {
            task=new Task(TASK_DESCRIPTION, Task.getDateFormat().parse("2021-02-12 10:10"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    void testTaskGetDescriptionValid() {
        assertEquals(TASK_DESCRIPTION, task.getDescription());
    }

    @Test
    void testTaskSetDescriptionValid() {
        String text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pulvinar nisl ac semper mattis. In at neque mattis, rhoncus lorem quis, faucibus augue.";
        task.setDescription(text);
        assertEquals(text, task.getDescription());
    }

    @Test
    @Disabled("not our test")
    void testTaskCreation() throws ParseException {
       assert task.getDescription() == "new task";
        System.out.println(task.getFormattedDateStart());
        System.out.println(task.getDateFormat().format(Task.getDateFormat().parse("2021-02-12 10:10")));
       assert task.getFormattedDateStart().equals(task.getDateFormat().format(Task.getDateFormat().parse("2021-02-12 10:10")));
    }

    @AfterEach
    void tearDown() {
    }
}