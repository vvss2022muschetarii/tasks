package tasks.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;
import org.junit.jupiter.api.Assertions;

import org.mockito.Mockito;

public class ArrayTaskListUnitTest {
    private ArrayTaskList arrayTaskList;

    private Task task;

    @BeforeEach
    void setUp() {
        arrayTaskList = new ArrayTaskList();
        task = Mockito.mock(Task.class);
    }

    @Test
    void testAddValid() {
        arrayTaskList.add(task);
        Assertions.assertEquals(1, arrayTaskList.size());
    }

    @Test
    void testRemoveInvalid() {
        boolean removed = arrayTaskList.remove(task);
        Assertions.assertFalse(removed);
        Assertions.assertEquals(0, arrayTaskList.size());
    }

    @Test
    public void testRemoveValid() {
        Task task = new Task("task", new Date());
        arrayTaskList.add(task);
//        Mockito.when(task.equals(Mockito.any(Task.class))).thenReturn(true);
        boolean removed = arrayTaskList.remove(task);
        Assertions.assertTrue(removed);
        Assertions.assertEquals(0, arrayTaskList.size());
    }
}
